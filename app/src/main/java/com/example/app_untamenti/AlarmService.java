package com.example.app_untamenti;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.Calendar;

import static java.lang.System.currentTimeMillis;

public class AlarmService extends Service {

    private static final String NOTIFICATION_CHANNEL_ID = "CHANNEL_SERVICE";
    private static final int DAY = 0;
    private static final int MONTH = 1;
    private static final int YEAR = 2;
    private static final int HOUR = 0;
    private static final int MINUTE = 1;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String msg = intent.getStringExtra("text");
        String phoneNo = intent.getStringExtra("phone");
        String date = intent.getStringExtra("date");
        String hour = intent.getStringExtra("hour");
        int id = intent.getIntExtra("id", -1);

        String[] dayMonthYear = date.split("/");
        String[] hourMinute = hour.split(":");

        Intent alarmIntent = new Intent(getApplicationContext(), Receiver.class);

        alarmIntent.putExtra("text", msg);
        alarmIntent.putExtra("phone", phoneNo);
        alarmIntent.putExtra("hour", hour);
        alarmIntent.putExtra("data", date);
        alarmIntent.putExtra("id", id);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                                                                id,
                                                                alarmIntent,
                                                                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        /* Set the alarm to start at 10:30 AM */
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentTimeMillis());
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayMonthYear[DAY]));
        calendar.set(Calendar.MONTH, Integer.parseInt(dayMonthYear[MONTH]) - 1);
        calendar.set(Calendar.YEAR, Integer.parseInt(dayMonthYear[YEAR]));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourMinute[HOUR]) - 1);
        calendar.set(Calendar.MINUTE, Integer.parseInt(hourMinute[MINUTE]));
        calendar.set(Calendar.SECOND, 0);

        this.setBackgroundService();
        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        return START_NOT_STICKY;
    }

    private void setBackgroundService(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_ID, NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setDescription(NOTIFICATION_CHANNEL_ID);
            notificationChannel.setSound(null, null);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setOngoing(false)
                .setSmallIcon(R.drawable.ic_baseline_access_alarms_24)
                .setPriority(NotificationManager.IMPORTANCE_MIN);
        startForeground(1, builder.build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
