package com.example.app_untamenti;

import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.example.app_untamenti.ViewModel.ListEventViewModel;
import com.squareup.seismic.ShakeDetector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateCalendarFragment extends Fragment {

    private static final String LOG = "DataCalendarFragment";
    private static final int FIRST_TEN_DAYS_MONTH = 10;
    private final static int TOAST_TIME = 1300;
    private ListEventViewModel model;
    private int id;
    private SensorManager sm;
    private ShakeDetector sd;
    private boolean switch_mode;
    private List<Calendar> calendars;
    private CalendarView calendarView;
    private Toast toast;
    private boolean resume;

    public DateCalendarFragment(final boolean mode){
        this.switch_mode = mode;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.sd.start(sm);
        if(resume){
            Log.d("calendars", "Checkmode in resume");
            this.checkMode();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        sd.stop();
        this.toast.cancel();
        Log.d("calendars", "Sono in pause()");
        this.resume = true;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_calendar, container, false);
        if(this.calendars == null){
            this.calendars = new ArrayList<>();
        }
        this.model = new ViewModelProvider(getActivity()).get(ListEventViewModel.class);
        this.calendarView = view.findViewById(R.id.calendarView);
        //when the list of the items changed, the adapter gets the new list.
        model.getEvents().observe(getActivity(), new Observer<List<CardEvent>>() {
            @Override
            public void onChanged(List<CardEvent> cardEvents) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                calendars.clear();
                for (CardEvent event : cardEvents) {
                    //get maximum id
                    if(event.getEvent_id() > id){
                        id = event.getEvent_id();
                    }
                    Date temp = null;
                    try {
                        temp = sdf.parse(event.getDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(temp);
                    calendars.add(cal);
                }
                //this is new id
                id += 1;
                if(!resume){
                    Log.d("calendars", "Checkmode nel model");
                    resume = false;
                    checkMode();
                }
            }
        });
        sd = new ShakeDetector(() -> {
            resume = true;
            if(switch_mode){
                switch_mode = false;
                Log.d("calendars", "switch true");
            } else {
                switch_mode = true;
                Log.d("calendars", "switch false");
            }
            Log.d("calendars", "checkMode nello shaker");
            this.checkMode();
        });
        sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        /*Button button = view.findViewById(R.id.shake);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!one_time_shaker){
                    one_time_shaker = true;
                    DateCalendarFragment.this.toastDisplay(R.string.toastMessage1);
                    calendarView.setSelectedDates(new ArrayList<>());
                    DateCalendarFragment.this.setDefaultBehaviour(calendarView);
                } else {
                    one_time_shaker = false;
                    DateCalendarFragment.this.toastDisplay(R.string.toastMessage2);
                    calendarView.setSelectedDates(calendars);
                    calendarView.setOnDayClickListener(new OnDayClickListener() {
                        @Override
                        public void onDayClick(EventDay eventDay) {
                            Calendar clickedDayCalendar = eventDay.getCalendar();
                            int year = clickedDayCalendar.get(Calendar.YEAR);
                            int month = clickedDayCalendar.get(Calendar.MONTH);
                            int day = clickedDayCalendar.get(Calendar.DAY_OF_MONTH);
                            Log.d("ola", "" + clickedDayCalendar.getTime());
                            FragmentActivity activity = getActivity();
                            if(activity != null) {
                                final String date = calculateDate(year, month, day);
                                final Intent intent = new Intent(getContext(), MainActivity.class);
                                intent.putExtra("Date", date);
                                intent.putExtra("ID", id);
                                activity.startActivity(intent);
                            }
                        }
                    });
                }
            }
        });*/

        return view;
    }

    private void checkMode(){
        if(switch_mode){
            this.setAdd_mode();
        } else {
            this.setView_mode();
        }
    }

    private void setAdd_mode(){
        this.toastDisplay(R.string.toastMessage1);
        calendarView.setSelectedDates(new ArrayList<>());
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar clickedDayCalendar = eventDay.getCalendar();
                int year = clickedDayCalendar.get(Calendar.YEAR);
                int month = clickedDayCalendar.get(Calendar.MONTH);
                int day = clickedDayCalendar.get(Calendar.DAY_OF_MONTH);
                String date = calculateDate(year, month, day);
                if(checkDate(date)) {
                    showDialog(date, id);
                } else {
                    calendarView.setDisabledDays(Arrays.asList(clickedDayCalendar));
                }
            }
        });
    }

    private void setView_mode(){
        this.toastDisplay(R.string.toastMessage2);
        Log.d("calendars", calendars.toString());
        calendarView.setSelectedDates(calendars);
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar clickedDayCalendar = eventDay.getCalendar();
                int year = clickedDayCalendar.get(Calendar.YEAR);
                int month = clickedDayCalendar.get(Calendar.MONTH);
                int day = clickedDayCalendar.get(Calendar.DAY_OF_MONTH);
                Log.d("ola", "" + clickedDayCalendar.getTime());
                FragmentActivity activity = getActivity();
                if(activity != null) {
                    final String date = calculateDate(year, month, day);
                    if(checkDate(date)){
                        final Intent intent = new Intent(getContext(), MainActivity.class);
                        intent.putExtra("Date", date);
                        intent.putExtra("ID", id);
                        activity.startActivity(intent);
                    } else {
                        calendarView.setDisabledDays(Arrays.asList(clickedDayCalendar));
                    }
                }
            }
        });
    }

    private void toastDisplay(final int id){
        if(this.toast != null){
            this.toast.cancel();
        }
        this.toast = Toast.makeText(getContext().getApplicationContext(), id, Toast.LENGTH_SHORT);
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) getActivity().findViewById(R.id.custom_toast_container));
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(id);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, TOAST_TIME);
    }

    private boolean checkDate(String date) {
        String todayDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        boolean check = true;
        String[] partsDate = date.split("/");
        String[] partsTodayDate = todayDate.split("/");

        if(Integer.parseInt(partsDate[2]) < Integer.parseInt(partsTodayDate[2])) { //Year
            Log.d(LOG, "Year");
            check = false;
        } else if(Integer.parseInt(partsDate[2]) == Integer.parseInt(partsTodayDate[2]) && //Year equal
                Integer.parseInt(partsDate[1]) < Integer.parseInt(partsTodayDate[1])) { //Month
            Log.d(LOG, "Month");
            check = false;
        } else if (Integer.parseInt(partsDate[2]) == Integer.parseInt(partsTodayDate[2]) &&
                Integer.parseInt(partsDate[1]) == Integer.parseInt(partsTodayDate[1]) &&
                Integer.parseInt(partsDate[0]) < Integer.parseInt(partsTodayDate[0])) { //Day
            Log.d(LOG, "Day");
            check = false;
        }

        return check;
    }

    private String calculateDate(final int year, int month, final int dayOfMonth){
        month += 1;
        Log.d(LOG, Integer.toString(month) + " " + dayOfMonth);
        String date = "";
        if(dayOfMonth < FIRST_TEN_DAYS_MONTH){
            date = "0" + dayOfMonth;
        } else {
            date = "" + dayOfMonth;
        }
        if(month < FIRST_TEN_DAYS_MONTH){
            date += "/0" + month;
        } else {
            date += "/" + month;
        }
        date += "/"+year;
        return date;
    }

    private void showDialog(final String date, final int id) {
        DialogFragment dialog = new addDateFragment(date, id);
        // The device is smaller, so show the fragment fullscreen
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        this.toast.cancel();
        transaction.replace(R.id.fragment_container, dialog)
                .addToBackStack(null).commit();
    }
}
