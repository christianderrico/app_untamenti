package com.example.app_untamenti;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends AppCompatActivity {

    private static final String FRAGMENT_TAG = "homeFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //insert the layout in the Activity
        setContentView(R.layout.activity_main);
        final String date = getIntent().getStringExtra("Date");
        final int id = getIntent().getIntExtra("ID", 0);
        //the application is just started (never gone on onStop())
        if (savedInstanceState == null) {
            Utility.insertFragment(this, new HomeFragment(date, id), FRAGMENT_TAG);
        }
    }

    /**
     *
     * @param requestCode requestCode of the intent (ACTIVITY_ADD_TRIP in this case)
     * @param resultCode the result of the intent (RESULT_OK)
     * @param data the optional data in the intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("code", "onActivityResult()");
        if (requestCode == Utility.ACTIVITY_ADD_EVENT && resultCode == RESULT_OK) {
            Log.d("code", "RESULT_OK");
            FragmentManager manager = getSupportFragmentManager();
            HomeFragment homeFragment = (HomeFragment) manager.findFragmentByTag(FRAGMENT_TAG);
            if (homeFragment != null) {
                Log.d("code", "updateList()");
                homeFragment.updateList();
            }
        }
    }
}
