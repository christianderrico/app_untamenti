package com.example.app_untamenti;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.telephony.SmsManager;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import static com.example.app_untamenti.DateCalendarActivity.CHANNEL_ID;


public class Receiver extends  BroadcastReceiver{

    private static final int NOTIFICATION_ID = 0;
    private static final boolean MODE_VIEW = false;
    private static final int AWAKEN_TIME_MILLISECONDS = 1000;

    @Override
    public void onReceive(Context context, Intent intent) {
        String phoneNo = intent.getStringExtra("phone");
        String message = intent.getStringExtra("text");
        String hour = intent.getStringExtra("hour");
        String date = intent.getStringExtra("date");
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNo, null, message, null, null);
        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(context, DateCalendarActivity.class);
        resultIntent.putExtra("mode", MODE_VIEW);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isInteractive(); // check if screen is on
        if (!isScreenOn) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myApp:notificationLock");
            wl.acquire(AWAKEN_TIME_MILLISECONDS); //set your time in milliseconds
        }

        Notification noti = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle("Appuntamento ore " + hour)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(NOTIFICATION_ID, noti);
    }

}
