package com.example.app_untamenti.RecyclerView;

import android.app.Activity;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app_untamenti.CardEvent;
import com.example.app_untamenti.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter linked to the RecyclerView of the homePage, that extends a custom ViewHolder and
 * implements Filterable for the SearchView
 */
public class CardAdapter extends RecyclerView.Adapter<CardViewHolder> implements Filterable {

    private static final String LOG = "CardAdapter";
    //list that can be filtered
    private List <CardEvent> cardEventList = new ArrayList<>();
    //list that contains ALL the element added by the user
    private List <CardEvent> cardEventListFull = new ArrayList<>();

    //listener attached to the onclick event for the item in yhe RecyclerView
    private OnItemListener listener;

    //i need the activity to get the drawable for the image
    private Activity activity;

    public CardAdapter(OnItemListener listener, Activity activity) {
        this.listener = listener;
        this.activity = activity;
    }

    /**
     *
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     *
     * @param parent ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,
                parent, false);
        return new CardViewHolder(layoutView, listener);
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * This method should update the contents of the RecyclerView.ViewHolder.itemView to reflect
     * the item at the given position.
     *
     * @param holder ViewHolder which should be updated to represent the contents of the item at
     *               the given position in the data set.
     * @param position position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        String phone = activity.getResources().getString(R.string.Phone);
        String hour = activity.getResources().getString(R.string.Hour);
        final CardEvent currentCardItem = cardEventList.get(position);
        holder.nameTextView.setText(currentCardItem.getName_user());
        holder.dateTextView.setText(currentCardItem.getDate());
        holder.telephoneTextView.setText(phone + " " + currentCardItem.getTelephone());
        holder.noteCard.setText(currentCardItem.getNote());
        holder.startHour.setText(hour + " " + currentCardItem.getStart_hour());
    }

    @Override
    public int getItemCount() {
        return cardEventList.size();
    }

    /**
     * Method called when you have to filter a list (in our case the one with the trip
     * @return  filter that can be used to constrain data with a filtering pattern.
     *
     */
    @Override
    public Filter getFilter() {
        return cardFilter;
    }

    private Filter cardFilter = new Filter() {
        /**
         * Called to filter the data according to the constraint
         * @param constraint constraint used to filtered the data
         * @return the result of the filtering
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<CardEvent> filteredList = new ArrayList<>();

            //if you have no constraint --> return the full list
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(cardEventListFull);
            } else {
                //else apply the filter and return a filtered list
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (CardEvent event : cardEventListFull) {
                    if (event.getName_user().toLowerCase().contains(filterPattern)
                            || event.getStart_hour().contains(filterPattern)) {
                        filteredList.add(event);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        /**
         * Called to publish the filtering results in the user interface
         * @param constraint constraint used to filter the data
         * @param results the result of the filtering
         */
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cardEventList.clear();
            //cardEventListFull.addAll((List) results.values);
            List<?> result = (List<?>) results.values;
            for (Object object : result) {
                if (object instanceof CardEvent) {
                    cardEventList.add((CardEvent) object);
                }
            }
            //warn the adapter that the dare are changed after the filtering
            notifyDataSetChanged();
        }
    };

    /**
     * Method called when a new event is added
     * @param newData the new list of events
     */
    public void setData(List<CardEvent> newData) {
        Log.d(LOG, "setData()");
        this.cardEventList.clear();
        this.cardEventList.addAll(newData);
        this.cardEventListFull.clear();
        this.cardEventListFull.addAll(newData);
        notifyDataSetChanged();
    }

    public void deleteData(int position) {
        cardEventListFull.remove(position);
        cardEventList.remove(position);
        Log.d(LOG, cardEventListFull.size() + " " + cardEventList.size());
    }

    public CardEvent getEvent(int position) {
        return this.cardEventListFull.get(position);
    }

    public List<CardEvent> getAllCards() { return this.cardEventListFull; }
}