package com.example.app_untamenti.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app_untamenti.R;

/**
 * A ViewHolder describes an item view and the metadata about its place within the RecyclerView.
 * Every item in the list has a listener for the onclick event
 */
class CardViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

    ImageView imageCardView;
    TextView nameTextView;
    TextView telephoneTextView;
    TextView dateTextView;
    TextView noteCard;
    TextView startHour;
    ImageView deleteImage;
    ImageView editImage;
    private OnItemListener itemListener;

    CardViewHolder(@NonNull View itemView, final OnItemListener lister) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.nameTextView);
        telephoneTextView = itemView.findViewById(R.id.telephoneTextView);
        dateTextView = itemView.findViewById(R.id.dateTextView);
        noteCard = itemView.findViewById(R.id.noteTextView);
        startHour = itemView.findViewById(R.id.startHourTextView);
        deleteImage = itemView.findViewById(R.id.trash);
        editImage = itemView.findViewById(R.id.edit);
        itemListener = lister;

        deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lister != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        lister.onDeleteClick(position);
                    }
                }

            }
        });

        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lister != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        lister.onEditClick(position);
                    }
                }

            }
        });

        itemView.setOnClickListener(this);
    }

    /**
     * implementation of method in View.OnclickListener
     * @param v the view
     */
    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAdapterPosition());
    }
}