package com.example.app_untamenti.RecyclerView;

/**
 * Interface to manage the listener for the click on an element of the RecyclerView
 */
public interface OnItemListener{
    void onItemClick(int position);

    void onDeleteClick(int position);

    void onEditClick(int position);
}