package com.example.app_untamenti;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.example.app_untamenti.ViewModel.EditEventViewModel;
import com.example.app_untamenti.ViewModel.MyTextWatcher;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Arrays;

public class addDateFragmentEdit extends DialogFragment {
    private static final String LOG = "FragmentEdit";

    private static final String FRAGMENT_TAG = "homeFragment";

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0 ;

    private EditEventViewModel editModel;

    private EditText text;
    private EditText name_user;
    private EditText telephone;
    private EditText start_hour;
    private EditText note;
    private Button button;

    private int id;
    private String date;
    private String name;
    private String hour;
    private String phone;
    private String note2;

    public addDateFragmentEdit(final int id, final String name, final String date, final String hour, final String telephone, final String note){
        this.date = date;
        this.id = id;
        this.name = name;
        this.hour = hour;
        this.phone = telephone;
        this.note2 = note;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        final View view = inflater.inflate(R.layout.dialog_desing, container, false);
        this.text = view.findViewById(R.id.dateTextField);
        this.name_user = view.findViewById(R.id.editTextTextPersonName);
        this.telephone = view.findViewById(R.id.editTextPhone);
        this.start_hour = view.findViewById(R.id.editTextStartHour);
        this.note = view.findViewById(R.id.editTextNotes);
        this.button = view.findViewById(R.id.addEventButton);


        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.SEND_SMS},
                MY_PERMISSIONS_REQUEST_SEND_SMS);

        text.setText(date);
        name_user.setText(name);
        telephone.setText(phone);
        start_hour.setText(hour);
        note.setText(note2);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(LOG, "ID: " + Integer.toString(this.id));
        final FragmentActivity activity = getActivity();
        if(activity != null) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            // Set up the toolbar
            Utility.setUpToolbar((AppCompatActivity) getActivity(), getString(R.string.edit_event));
            button.setText(getString(R.string.save));

            //final TextView errorText = activity.findViewById(R.id.errorTextView);

            activity.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(LOG, "CANCEL BUTTON");
                    activity.onBackPressed();
                }
            });


            editModel = new ViewModelProvider(activity).get(EditEventViewModel.class);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(LOG, "EDIT EVENT BUTTON");

                    if(checkItems()) { //Check if fields are empty
                        String user = name_user.getText().toString();
                        String phone = telephone.getText().toString();
                        String startHour = start_hour.getText().toString();
                        String _note = note.getText().toString();

                        if(parseStartHour()) { //Check start hour syntax
                            Log.d(LOG, "User: " + user + " phone: " + phone + " date: " + date + " start hour: " + startHour + " note: " + note);
                            sendSMSMessage();
                            editModel.editEvent(addDateFragmentEdit.this.id, user, phone, startHour, _note);

                            FragmentManager manager = activity.getSupportFragmentManager();
                            HomeFragment homeFragment = (HomeFragment) manager.findFragmentByTag(FRAGMENT_TAG);
                            if (homeFragment != null) {
                                Log.d(LOG, "updateList()");
                                homeFragment.updateList();
                                activity.onBackPressed();
                            }
                        }
                    }
                }
            });
        }
    }

    private boolean checkItems(/*final TextView textView*/) {
        final FragmentActivity activity = getActivity();
        boolean check = true;
        if(!inputNotEmpty(name_user.getText())) {
            TextInputLayout name = activity.findViewById(R.id.textFieldPersonName);
            name.setError(getString(R.string.name_empty));
            TextInputEditText nameAct = activity.findViewById(R.id.editTextTextPersonName);
            nameAct.addTextChangedListener(new MyTextWatcher(name));
            check = false;
        }
        if(!inputNotEmpty(telephone.getText())) {
            TextInputLayout telephone = activity.findViewById(R.id.fieldPhone);
            telephone.setError(getString(R.string.telephone_empty));
            TextInputEditText phoneAct = activity.findViewById(R.id.editTextPhone);
            phoneAct.addTextChangedListener(new MyTextWatcher(telephone));
            check = false;
        }
        if(!inputNotEmpty(start_hour.getText())) {
            TextInputLayout hour = activity.findViewById(R.id.fieldHour);
            hour.setError(getString(R.string.start_hour_empty));
            TextInputEditText hourAct = activity.findViewById(R.id.editTextStartHour);
            hourAct.addTextChangedListener(new MyTextWatcher(hour));
            check = false;
        }
        return check;
    }

    private boolean parseStartHour() {
        boolean check = true;
        final FragmentActivity activity = getActivity();
        TextInputLayout hour = activity.findViewById(R.id.fieldHour);
        String[] parts = start_hour.getText().toString().split(":");
        Log.d(LOG, Arrays.toString(parts));
        if (parts.length == 0 || parts.length == 1) {
            hour.setError(getString(R.string.separator_hour));
            TextInputEditText hourAct = activity.findViewById(R.id.editTextStartHour);
            hourAct.addTextChangedListener(new MyTextWatcher(hour));
            check = false;
        } else {
            if(Integer.parseInt(parts[0]) > 23 || Integer.parseInt(parts[0]) < 0) {
                hour.setError(getString(R.string.hour_error));
                TextInputEditText hourAct = activity.findViewById(R.id.editTextStartHour);
                hourAct.addTextChangedListener(new MyTextWatcher(hour));
                check = false;
            } else if(Integer.parseInt(parts[1]) < 0 || Integer.parseInt(parts[1]) > 59) {
                hour.setError(getString(R.string.minutes_error));
                TextInputEditText hourAct = activity.findViewById(R.id.editTextStartHour);
                hourAct.addTextChangedListener(new MyTextWatcher(hour));
                check = false;
            }
        }
        return check;
    }

    private boolean inputNotEmpty(@Nullable Editable text) {
        return text != null && text.length() > 0;
    }

    /** The system calls this only when creating the layout in a dialog. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void start() {

        String message = "Il servizio AppPuntamenti ti ricorda " +
                "che alle ore: " + this.start_hour.getText().toString() + " hai fissato un appuntamento";

        Intent serviceIntent = new Intent(getActivity(), AlarmService.class);

        serviceIntent.putExtra("text", message);
        serviceIntent.putExtra("phone", this.telephone.getText().toString());
        serviceIntent.putExtra("date", this.date);
        serviceIntent.putExtra("hour", this.start_hour.getText().toString());

        getActivity().startService(serviceIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start();
                } else {
                    Toast.makeText(getActivity(),
                            "SMS failed, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

    }

    private void sendSMSMessage() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.SEND_SMS)
                == PackageManager.PERMISSION_GRANTED) {
            start();
        }
    }
}
