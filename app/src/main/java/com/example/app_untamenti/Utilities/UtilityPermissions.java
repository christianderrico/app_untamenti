package com.example.app_untamenti.Utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.example.app_untamenti.BuildConfig;
import com.example.app_untamenti.R;
import com.google.android.material.snackbar.Snackbar;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class UtilityPermissions {

    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 0;
    private static final String REQUEST_PERMISSIONS = Manifest.permission.ACCESS_FINE_LOCATION;

    public static boolean checkPermissions(Activity activity) {
        int permissionState = ActivityCompat.checkSelfPermission(activity, REQUEST_PERMISSIONS);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public static void createSnackBar(final Activity activity) {
        // Notify the user via a SnackBar that they have rejected a core permission for the
        // app, which makes the Activity useless.
        Snackbar.make(
                activity.findViewById(R.id.fragment_container),
                R.string.permission_denied_explanation,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Build intent that displays the App settings screen.
                        Intent intent = new Intent();
                        intent.setAction(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",
                                BuildConfig.APPLICATION_ID, null);
                        intent.setData(uri);
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                    }
                })
                .show();
    }

}
