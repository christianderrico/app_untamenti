package com.example.app_untamenti.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.app_untamenti.CardEvent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Database(entities = {CardEvent.class}, version = 1, exportSchema = false)
abstract class CardEventDatabase extends RoomDatabase {

    abstract CardEventDAO cardEventDAO();

    //Singleton instance
    private static volatile CardEventDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;

    //ExecutorService with a fixed thread pool that you will use to run database operations
    // asynchronously on a background thread.
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    //get the singleton instance
    static CardEventDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CardEventDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CardEventDatabase.class, "event_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
