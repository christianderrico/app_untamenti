package com.example.app_untamenti.Database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.app_untamenti.CardEvent;

import java.util.List;

public class CardEventRepository {
    private CardEventDAO cardEventDAO;
    private int id;

    public CardEventRepository(Application application) {
        CardEventDatabase db = CardEventDatabase.getDatabase(application);
        cardEventDAO = db.cardEventDAO();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<CardEvent>> getEvent(){
        return cardEventDAO.getEvent();
    }


    public LiveData<List<CardEvent>> getEvent(String dateSearch) {
        return cardEventDAO.getEvent(dateSearch);
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public void addCardEvent(final CardEvent card) {
        CardEventDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                cardEventDAO.addCardEvent(card);
            }
        });
    }

    public void deleteEvent(final CardEvent event) {
        CardEventDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                cardEventDAO.deleteEvent(event);
            }
        });
    }

    public int getMaxId() {
        CardEventDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                id =  cardEventDAO.getMaxIdEvent();
            }
        });
        return id;
    }

    public void editEvent(final int id, final String name, final String telephone, final String hour, final String note) {
        CardEventDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                cardEventDAO.editEvent(id, name, telephone, hour, note);
            }
        });
    }
}
