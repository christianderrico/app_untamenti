package com.example.app_untamenti.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.app_untamenti.CardEvent;

import java.util.List;

@Dao
public interface CardEventDAO {
    // The selected on conflict strategy ignores a new CardItem
    // if it's exactly the same as one already in the list.
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addCardEvent(CardEvent cardEvent);

    @Transaction
    @Query("SELECT * from event ORDER BY date ASC")
    LiveData<List<CardEvent>> getEvent();

    @Transaction
    @Query("SELECT * from event WHERE date = :dateSearch")
    LiveData<List<CardEvent>> getEvent(String dateSearch);

    @Transaction
    @Delete()
    void deleteEvent(CardEvent event);

    @Query("SELECT MAX(event_id) AS max_id FROM event")
    int getMaxIdEvent();

    @Query("UPDATE event SET name_user = :name,  telephone = :phone, start_hour = :hour, note = :note WHERE event_id = :id")
    void editEvent(int id, String name, String phone, String hour, String note);
}
