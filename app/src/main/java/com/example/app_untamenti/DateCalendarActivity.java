package com.example.app_untamenti;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class DateCalendarActivity extends AppCompatActivity {

    private static final String FRAGMENT_TAG = "DateCalendarFragment";
    public static final String CHANNEL_ID = "channel_1";
    //private GetMaxEventID idSelector;
    //private int maxID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean mode = true;
        setContentView(R.layout.activity_calendar);
        if(getIntent() != null){
            if(getIntent().hasExtra("mode")){
                mode = getIntent().getBooleanExtra("mode", true);
            }
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (savedInstanceState == null) {
            Utility.insertFragment(this, new DateCalendarFragment(mode), FRAGMENT_TAG);
        }
        this.createNotificationChannel();
    }


    private void createNotificationChannel(){
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
