package com.example.app_untamenti;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class which represents an event
 */
@Entity(tableName = "event")
public class CardEvent {
    @PrimaryKey
    @ColumnInfo(name = "event_id")
    private int event_id;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "telephone")
    private String telephone;

    @ColumnInfo(name = "name_user")
    private String name_user;

    @ColumnInfo(name = "start_hour")
    private String start_hour;

    @ColumnInfo(name = "note")
    private String note;

    public CardEvent(String name_user, String note, String date, String start_hour, String telephone, int event_id) {
        this.name_user = name_user;
        this.note = note;
        this.date = date;
        this.start_hour = start_hour;
        this.telephone = telephone;
        this.event_id = event_id;
    }

    public String getNote() {
        return note;
    }

    public String getDate() {
        return date;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int id) {
        this.event_id = id;
    }

    public String getName_user() { return name_user; }

    public String getStart_hour() { return start_hour; }

    public String getTelephone() { return telephone; }

    public void setName_user(String name_user) { this.name_user = name_user; }

    public void setStart_hour(String start_hour) { this.start_hour = start_hour; }

    public void setTelephone(String telephone) { this.telephone = telephone; }

    public void setNote(String note) { this.note = note; }
}
