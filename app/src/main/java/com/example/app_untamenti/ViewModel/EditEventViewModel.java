package com.example.app_untamenti.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.app_untamenti.Database.CardEventRepository;

public class EditEventViewModel extends AndroidViewModel {

    private CardEventRepository repository;

    public EditEventViewModel(@NonNull Application application) {
        super(application);
        repository = new CardEventRepository(application);
    }

    public void editEvent(final int id, final String name, final String telephone, final String hour, final String note) {
        repository.editEvent(id, name, telephone, hour, note);
    }
}
