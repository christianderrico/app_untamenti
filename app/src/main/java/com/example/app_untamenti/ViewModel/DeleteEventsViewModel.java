package com.example.app_untamenti.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.app_untamenti.CardEvent;
import com.example.app_untamenti.Database.CardEventRepository;

public class DeleteEventsViewModel extends AndroidViewModel {
    private CardEventRepository repository;

    public DeleteEventsViewModel(@NonNull Application application) {
        super(application);
        this.repository = new CardEventRepository(application);
    }

    public void deleteEvent(CardEvent event) {
        repository.deleteEvent(event);
    }
}
