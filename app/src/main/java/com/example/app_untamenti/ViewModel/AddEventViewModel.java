package com.example.app_untamenti.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.app_untamenti.CardEvent;
import com.example.app_untamenti.Database.CardEventRepository;

/**
 * The ViewModel class is designed to store and manage UI-related data in a lifecycle conscious way.
 * The ViewModel class allows data to survive configuration changes such as screen rotations.
 *
 * The data stored by ViewModel are not for long term. (Until activity is destroyed)
 */
public class AddEventViewModel extends AndroidViewModel {

    private CardEventRepository repository;

    public AddEventViewModel(@NonNull Application application) {
        super(application);
        repository = new CardEventRepository(application);
    }

    public void addEvent(CardEvent event){
        repository.addCardEvent(event);
    }
}
