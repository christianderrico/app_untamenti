package com.example.app_untamenti.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.app_untamenti.CardEvent;
import com.example.app_untamenti.Database.CardEventRepository;

import java.util.List;

public class GetEventDate extends AndroidViewModel {
    private CardEventRepository repository;

    public GetEventDate(@NonNull Application application) {
        super(application);
        repository = new CardEventRepository(application);
    }

    public LiveData<List<CardEvent>> getEvent(String date) {
        return repository.getEvent(date);
    }
}
