package com.example.app_untamenti.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.app_untamenti.CardEvent;
import com.example.app_untamenti.Database.CardEventRepository;

import java.util.List;

/**
 * The ViewModel class is designed to store and manage UI-related data in a lifecycle conscious way.
 * The ViewModel class allows data to survive configuration changes such as screen rotations.
 *
 * The data stored by ViewModel are not for long term. (Until activity is destroyed)
 */
public class ListEventViewModel extends AndroidViewModel {

    private LiveData<List<CardEvent>> event_list;

    public ListEventViewModel(@NonNull Application application) {
        super(application);
        CardEventRepository repository = new CardEventRepository(application);
        event_list = repository.getEvent();
    }

    public LiveData<List<CardEvent>> getEvents() {
        return event_list;
    }
}
