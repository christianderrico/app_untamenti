package com.example.app_untamenti.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.app_untamenti.Database.CardEventRepository;

public class GetMaxEventID extends AndroidViewModel {

    private CardEventRepository repository;

    public GetMaxEventID(@NonNull Application application) {
        super(application);
        repository = new CardEventRepository(application);
    }

    public int getMaxId() {
        return repository.getMaxId();
    }
}
