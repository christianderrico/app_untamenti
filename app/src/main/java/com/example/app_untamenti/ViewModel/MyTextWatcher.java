package com.example.app_untamenti.ViewModel;

import android.text.Editable;
import android.text.TextWatcher;

import com.google.android.material.textfield.TextInputLayout;

public class MyTextWatcher implements TextWatcher {

    private final TextInputLayout texlLay;

    public MyTextWatcher(final TextInputLayout layout){
        this.texlLay = layout;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        this.texlLay.setError("");
        this.texlLay.setErrorEnabled(false);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
