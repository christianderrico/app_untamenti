package com.example.app_untamenti;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

public class Utility {

    static final int ACTIVITY_ADD_EVENT= 1;
    static final int REQUEST_IMAGE_CAPTURE = 2;

    static void setUpToolbar(AppCompatActivity activity, String title) {
        Toolbar toolbar = activity.findViewById(R.id.app_bar);
        toolbar.setTitle(title);

        //Set a Toolbar to act as the ActionBar for the Activity
        activity.setSupportActionBar(toolbar);
    }

    static void insertFragment(AppCompatActivity activity, Fragment fragment, String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment, tag)
                .commit();
    }

    /*
    public static Bitmap getImageBitmap(Activity activity, Uri currentPhotoUri){
        ContentResolver resolver = activity.getApplicationContext()
                .getContentResolver();
        try {
            InputStream stream = resolver.openInputStream(currentPhotoUri);
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            Objects.requireNonNull(stream).close();
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/
}
