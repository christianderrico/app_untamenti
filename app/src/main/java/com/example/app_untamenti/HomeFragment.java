package com.example.app_untamenti;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app_untamenti.RecyclerView.CardAdapter;
import com.example.app_untamenti.RecyclerView.OnItemListener;
import com.example.app_untamenti.ViewModel.DeleteEventsViewModel;
import com.example.app_untamenti.ViewModel.GetEventDate;
import com.example.app_untamenti.ViewModel.ListEventViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Objects;

/**
 * Fragment included in the HomePage
 */
public class HomeFragment extends Fragment implements OnItemListener {

    private static final String LOG = "Home-Fragment";
    private CardAdapter adapter;
    private RecyclerView recyclerView;
    private GetEventDate model;
    private final String date;
    private int id;

    public HomeFragment(final String date){
        this.date = date;
    }

    public HomeFragment(final String date, final int id) {
        this.date = date;
        this.id = id;
    }
    /**
     * Called to have the fragment instantiate its user interface view.
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    /**
     * Called when the fragment's activity has been created and this fragment's view hierarchy instantiated.
     * It can be used to do final initialization once these pieces are in place,
     * such as retrieving views or restoring state.
     *
     * @param savedInstanceState If the fragment is being re-created from a previous saved state, this is the state.
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            // Set up the toolbar
            Utility.setUpToolbar((AppCompatActivity) getActivity(), getString(R.string.titledate) + " " + this.date);
            setRecyclerView(activity);
            this.model = new ViewModelProvider(activity).get(GetEventDate.class);
            //when the list of the items changed, the adapter gets the new list.
            model.getEvent(this.date).observe(activity, new Observer<List<CardEvent>>() {
                @Override
                public void onChanged(List<CardEvent> cardEvents) {
                    Log.d(LOG, "setData()");
                    adapter.setData(cardEvents);
                    id += 1;
                }
            });

            //add action for the FAB button
            FloatingActionButton floatingActionButton = activity.findViewById(R.id.fab_add);
            Log.d("ID2", "" + id);
            if(id == 0){
                floatingActionButton.hide();
            } else {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDialog(date, id);
                    }
                });
            }

        } else {
            Log.e(LOG, "Activity is null");
        }
    }

    private void showDialog(final String date, final int id) {
        DialogFragment dialog = new addDateFragment(date, id);
        // The device is smaller, so show the fragment fullscreen
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        transaction.replace(R.id.fragment_container, dialog)
                .addToBackStack(null).commit();
    }

    /**
     * Initialize the contents of the Fragment host's standard options menu.
     * You should place your menu items in to menu. For this method to be called,
     * you must have first called setHasOptionsMenu(boolean) in onCreateView.
     * @param menu, The options menu in which you place your items
     * @param inflater, the inflater
     */
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.toolbar_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            /**
             * Called when the user submits the query. This could be due to a key press on the keyboard
             * or due to pressing a submit button.
             * @param query the query text that is to be submitted
             * @return true if the query has been handled by the listener, false to let the
             * SearchView perform the default action. **/

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            /**
             * Called when the query text is changed by the user.
             * @param newText the new content of the query text field.
             * @return false if the SearchView should perform the default action of showing any
             * suggestions if available, true if the action was handled by the listener. **/

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    /**
     * Called to do initial creation of a fragment.
     * Note that this can be called while the fragment's activity is still in the process of being created.
     * @param savedInstanceState  If the fragment is being re-created from a previous saved state, this is the state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //show the Toolbar menu (the search icon in our case)
        setHasOptionsMenu(true);
    }


    /**
     * Method to set the RecyclerView and the relative adapter
     * @param activity the current activity
     */
    private void setRecyclerView(final Activity activity){
        // Set up the RecyclerView
        recyclerView = activity.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        final OnItemListener listener = this;
        adapter = new CardAdapter(listener, activity);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Method called from the MainActivity when the user add a new card
     */
    void updateList() {
        adapter.notifyDataSetChanged();
        /*List<CardEvent> list = model.getEvents().getValue();

        if(list != null){
            for (CardEvent val : list){
                if (val != null){
                    Log.d("code", "L'user e\': " + val.getName_user());
                    Log.d("code", "L'id e\': " + val.getEvent_id());
                }
            }
        }
        Log.d(LOG, "updateList()");*/
    }

    /**
     * Method called when the user click a card
     * @param position position of the item clicked
     */
    @Override
    public void onItemClick(final int position) {
        final View view = Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(position)).itemView;
        final CardView cardView = view.findViewById(R.id.single_card);
        final View expandableView = cardView.findViewById(R.id.expandableView);

        //transition to show the hidden layout of the card clicked
        TransitionManager.beginDelayedTransition(cardView, new Slide());
        expandableView.setVisibility(expandableView.getVisibility()== View.GONE ?
                View.VISIBLE : View.GONE );
    }
    /**
     * Method called when the user click trash
     * @param position position of the item clicked
     */
    @Override
    public void onDeleteClick(int position) {
        final FragmentActivity activity = getActivity();
        if(activity != null) {
            final CardEvent event = this.adapter.getEvent(position);
            DeleteEventsViewModel model = new ViewModelProvider(activity).get(DeleteEventsViewModel.class);
            model.deleteEvent(event);
            adapter.deleteData(position);
            recyclerView.removeViewAt(position);
            adapter.notifyItemRemoved(position);
            adapter.notifyItemRangeChanged(position, adapter.getAllCards().size());
            this.deletePendingIntent(activity, event);
        }
    }

    private void deletePendingIntent(final FragmentActivity activity, final CardEvent event){
        Intent alarmIntent = new Intent(activity.getApplicationContext(), Receiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity.getApplicationContext(),
                event.getEvent_id(),
                alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    /**
     * Method called when the user click edit image
     * @param position position of the item clicked
     */
    @Override
    public void onEditClick(int position) {
        final FragmentActivity activity = getActivity();
        if(activity != null) {
            final CardEvent event = this.adapter.getEvent(position);
            Log.d(LOG, Integer.toString(event.getEvent_id()));
            this.deletePendingIntent(activity, event);
            this.showDialog(event.getEvent_id(), event.getName_user(), event.getDate(), event.getStart_hour(), event.getTelephone(), event.getNote());
        }
    }

    private void showDialog(final int id, final String name, final String date, final String hour, final String telephone, final String note) {
        DialogFragment dialog = new addDateFragmentEdit(id, name, date, hour, telephone, note);
        // The device is smaller, so show the fragment fullscreen
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        transaction.replace(R.id.fragment_container, dialog)
                .addToBackStack(null).commit();
    }
}
